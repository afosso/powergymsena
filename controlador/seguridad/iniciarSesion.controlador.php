<?php
    class ControladorUsuario{
        static public function ctrIniciarSesion()
        {
            if(isset($_POST["txtUsuario"])){
                if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["txtUsuario"]) &&
                preg_match('/^[a-zA-Z0-9]+$/', $_POST["txtContrasenia"])){
                    $tabla = "usuario";
                    $item = "nombreUsuario";
                    $valor = $_POST["txtUsuario"];
                    $respuesta = ModeloUsuario::mdlMostrarUsuarios($tabla, $item, $valor);
                    if($respuesta["nombreUsuario"] == $_POST["txtUsuario"] &&
                        $respuesta["contrasenia"] == EncriptarPassMD5::Encriptar($_POST["txtContrasenia"]) ){
                        $_SESSION["iniciarSesion"] = "ok";
                        $_SESSION["nombreUsuario"] = $respuesta["nombreUsuario"];
                        $_SESSION["idUsuario"] = $respuesta["idUsuario"];
                        //$respuesta["administrador"] == '1' ? $_SESSION["administrador"] = "si" : $_SESSION["administrador"] = "no"; window.location = "inicio";

                        echo '<script>
                            window.location.assign("inicio");
                            
                        </script>';
                    }else{
                        echo '<br>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span class="alert-inner--text">
                                    Los datos de usuario son incorrectos.
                                </span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">X</span>
                                </button>
                            </div>';
                    }
                }
            }
        }
    }
?>