<script src="./js/seguridad/carpeta.js"></script>
    <div class="card">
        <div class="card-header">
            
            <div class="row">
                <button class="btn btn-icon btn-3 btn-primary btn-sm" type="button" onclick="nuevo();">
                    <span class="btn-inner--icon"><i class="material-icons">clear</i></span>
                    <span class="btn-inner--text">Nuevo</span>
                </button>
                <button class="btn btn-icon btn-3 btn-primary btn-sm" type="button" onclick="consultar();">
                    <span class="btn-inner--icon"><i class="material-icons">search</i></span>
                    <span class="btn-inner--text">Consultar</span>
                </button>
                <button class="btn btn-icon btn-3 btn-primary btn-sm" type="button" onclick="guardar();" id="btnGuardar">
                    <span class="btn-inner--icon"><i class="material-icons">save</i></span>
                    <span class="btn-inner--text">Guardar</span>
                </button>
                <button class="btn btn-icon btn-3 btn-primary btn-sm" type="button" onclick="modificar();" id="btnModificar" disabled>
                    <span class="btn-inner--icon"><i class="material-icons">update</i></span>
                    <span class="btn-inner--text">Modificar</span>
                </button>
            </div>
        </div>
        <div class="card-body">
            <input type="hidden" name="hiddenIdCarpeta" id="hiddenIdCarpeta">
            <h3 class="card-title"> <strong>   Gestionar Menú</strong></h3>
            <br>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="txtCodigo">Código</label>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-check"></i></span>
                    </div>
                    <input class="form-control" placeholder="Código" type="text" id="txtCodigo" name="txtCodigo">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="txtDescripcion">Descripción</label>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-chevron-right"></i></span>
                    </div>
                    <input class="form-control" placeholder="Descripción" type="text" id="txtDescripcion" name="txtDescripcion">
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="ddlEstado">Estado</label>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-chevron-right"></i></span>
                    </div>
                    
                    <select class="form-control" name="ddlEstado" id="ddlEstado">
                        <option value="-1">--Seleccione--</option>
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                    </select>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="ddlCarpeta">Menú Padre</label>
                    <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-chevron-right"></i></span>
                    </div>
                    
                    <select class="form-control" name="ddlMenuPadre" id="ddlMenuPadre">
                    </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Código</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Menú padre</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="datos">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>