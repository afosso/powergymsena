<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
        <ul>
            <li>
            <a href="https://www.creative-tim.com">
                Power Gym Sena - PGS
            </a>
            </li>
        </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, desarrollado por ADSI 1501561
        </div>
        <!-- your footer here -->
    </div>
</footer>